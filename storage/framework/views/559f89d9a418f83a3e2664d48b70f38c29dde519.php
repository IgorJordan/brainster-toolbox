<div id="signUpModal" class="modal fade new-modal" role="dialog">
    <div class="modal-dialog">
  
      <!-- Modal content-->

        <div class="modal-content">

            <div class="modal-body text-center">
                <button type="button" class="close modal-close m-signup" data-dismiss="modal"><i class="fas fa-times"></i></button>
                <h3 class="modal-title"><b>Welcome to Brainster tools</b></h3>
                <p class="sub grey">Continue with:</p>
                <div class="line"></div>

                <div class="social-btn sub">
                    <a href="" class="btn btn-google" role="button"><i class="fab fa-google"></i> Sign Up with Google</a>
                </div>
                <div class="second-row sub">
                    <div class="social"><a href="" class="btn btn-facebook" role="button"><i class="fab fa-facebook-square"></i> Facebook</a></div>
                    <div class="social"><a href="" class="btn btn-github" role="button"><i class="fab fa-github"></i> Github</a></div>
                </div>

                <div class="second-row sub">
                    <div class="span-line"></div>
                    <div class="or"><p class="grey">OR</p></div>
                    <div class="span-line"></div>
                </div>

                <form action="<?php echo e(route('register')); ?>" method="POST" class="sub">
                <?php echo csrf_field(); ?>
            
                <div class="form-group">
                    <input type="text" class="form-control" id="name" name="name" placeholder="Username" required>
                </div>

                <div class="form-group">
                    <input type="email" class="form-control" id="email" name="email" placeholder="E-mail address" required>
                </div>

                <div class="form-group">
                    <input type="password" class="form-control" id="password" name="password" placeholder="Password" required>
                </div>

                    <button type="submit" class="form-control btn btn-submit sub" id="signUp"><b>Create Account</b></button>
                </form>

                <p class="sub">Already have an account?<a href="#signInModal" data-toggle="modal" data-dismiss="modal"> Login</a></p>
            </div>
        </div>
  
    </div>
</div>
<?php /**PATH /var/www/html/brainsterprojects/resources/views/layouts/modal-signup.blade.php ENDPATH**/ ?>