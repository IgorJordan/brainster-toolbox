<footer>
    <div class="container-fluid text-center footer">
        <p><i class="far fa-copyright"></i> Copyright Brainster 2020, all rights reserved</p>
    </div>
</footer>

<!-- jQuery library -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.js"></script>
<!-- Latest compiled JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<!-- JQuery documentation -->

<script type="text/javascript">
//dynamical select option
$(document).ready(function () {
    $(document).on('change', '#tech', function() {
        var tech_id = $(this).val();
        var op = " ";
        $.ajax({
            type: 'get',
            url: "<?php echo e(route('dynamical')); ?>",
            data: {'id':tech_id},
            dataType: "json",
            success: function(data){
                for (var i = 0; i < data.length; i++){
                    op += '<option value="'+data[i].id+'">'+data[i].name+'</option>';
                }
                $('#version').append(op);
            },
            error: function(){
                console.log('error');
            },
        });
    });

    setTimeout(function() {
        $('#errorDiv').fadeOut('fast');
    }, 5000); // <-- time in milliseconds
});

</script>

<?php if(Request::is('/')): ?>
<script type="text/javascript">
    $('body').on('keyup', '#search', function(){
        var searchQuery = $(this).val();
        $.ajax({
            type: "POST",
            url: "<?php echo e(route('live_search')); ?>",
            data: {
                '_token': '<?php echo e(csrf_token()); ?>',
                searchQuery: searchQuery,
            },
            dataType: "json",
            success: function(res){
                var techList = '';
                $('#searchList').html('');

                $.each(res, function(index, tech){
                    techList =
                    '<a href="/courses/'+tech.id+'"><div class="col-md-4"><div class="card"><img class="card-logo" src="images/logos/'+tech.logo+'" alt="program_image"><span>'+tech.name+'</span></div></div></a>';

                    $('#searchList').append(techList);
                });
            }
        });
    });
</script>

<?php elseif(Request::is('courses/*')): ?>

<script type="text/javascript">
    $(document).ready(function(){


        // pagination
        var start = 0;
        var nb = 10;
        var end = start + nb;
        var length = $('.course-card').length;
        var list = $('.course-card');

        list.hide().filter(':lt('+(end)+')').show();

        $('.prev, .next').click(function(e){
        e.preventDefault();

        if( $(this).hasClass('prev') ){
            start -= nb;
        } else {
            start += nb;
        }

        if( start < 0 || start >= length ) start = 0;
        end = start + nb;

        if( start == 0 ) list.hide().filter(':lt('+(end)+')').show();
        else list.hide().filter(':lt('+(end)+'):gt('+(start-1)+')').show();
        });


        var parentDiv = [];
        $(".filterCourse > label").each((index, elem) => {
        parentDiv.push(elem.id);
        });

        for (let ver = 0; ver < parentDiv.length; ver++) {
            let element = parentDiv[ver];
            let newElement = $("."+element).length;
            $("#"+element+" span").html(newElement);

            $("#"+element+" input").on('click', function(){
                classTag = $('.'+element);
                showSome();
                if( classTag.length <= 10) {
                    $('#padBtn').css("display", "none");
                } else{
                    $('#padBtn').css("display", "block");
                }
                if($(this).is(':checked') == false){
                    defaultView();
                    $('#padBtn').css("display", "block");
                } else {
                    changeView();
                }
            });
        }

        function defaultView(){
            $(".course-card").css("display", "block");
        }

        function changeView() {
            $('.course-card').not(classTag).css("display", "none");
        }

        function showSome(){
            $('.course-card').css("display", "block");
        }
    });

</script>

<?php endif; ?>


</body>
</html><?php /**PATH /var/www/html/brainsterprojects/resources/views/layouts/footer.blade.php ENDPATH**/ ?>