<div id="submitModal" class="modal fade new-modal" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
  
      <!-- Modal content-->

        <div class="modal-content">

            <div class="modal-body text-center">
                <button type="button" class="close modal-close m-submit" data-dismiss="modal"><i class="fas fa-times"></i></button>
                <h3 class="modal-title">Submit new tutorial</h3>
                <div class="s-line"></div>
                <form action="<?php echo e(route('courseStore')); ?>" method="POST" class="sub">
                <?php echo csrf_field(); ?>
                
                <div class="form-group">
                    <input type="text" class="form-control" id="title" name="title" placeholder="Title" required>
                </div>
                <div class="form-group">
                    <input type="text" class="form-control" id="platform" name="platform" placeholder="Platform" required>
                </div>
                <div class="form-group">
                    <input type="text" class="form-control" id="url" name="url" placeholder="URL" required>
                </div>
                <div class="form-group">
                    <select class="form-control select-color" id="tech" name="tech" required>
                        <option value="" class="grey">Select technology</option>
                        <?php $__currentLoopData = $teches; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $tech): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <option value="<?php echo e($tech->id); ?>"><?php echo e($tech->name); ?></option>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </select>
                </div>
                <div class="form-group">
                    <select class="form-control select-color" id="type" name="type" required>
                        <option value="" class="grey">Select type of course</option>
                        <?php $__currentLoopData = $types; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $type): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <option value="<?php echo e($type->id); ?>"><?php echo e($type->name); ?></option>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </select>
                </div>
                <div class="form-group">
                    <select class="form-control select-color" id="medium" name="medium">
                        <option value="" class="grey">Select course medium</option>
                        <?php $__currentLoopData = $media; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $medium): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <option value="<?php echo e($medium->id); ?>"><?php echo e($medium->name); ?></option>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </select>
                </div>
                <div class="form-group">
                    <select class="form-control select-color" id="level" name="level">
                        <option value="" class="grey">Select course level</option>
                        <?php $__currentLoopData = $levels; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $level): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <option value="<?php echo e($level->id); ?>"><?php echo e($level->name); ?></option>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </select>
                </div>
                <div class="form-group">
                    <select class="form-control select-color" id="version" name="version">
                        <option value="">Select technology version</option>
                        
                    </select>
                </div>
                
                    <button onClick="myFunction()" type="submit" class="btn btn-submit form-control sub" id="save"><b>Save</b></button>
                </form>

            </div>
        </div>
  
    </div>
</div><?php /**PATH /var/www/html/brainsterprojects/resources/views/layouts/modal-submit.blade.php ENDPATH**/ ?>