<div id="loginModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
  
      <!-- Modal content-->

        <div class="modal-content">
            <div class="modal-header purple">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Пријави се и избери ја својата омилена категорија</h4>
            </div>
            <div class="modal-body">
                <form action="<?php echo e(route('newStudent')); ?>" method="POST">
                <?php echo csrf_field(); ?>
                <?php if(count($errors) > 0): ?>
                <div class="alert alert-danger">
                    <ul>
                    <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <li><?php echo e($error); ?></li>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </ul>
                </div>
                <?php elseif(Request::is('newStudent')): ?>
                <div class="alert alert-success">
                    <ul>
                        <li>The lesson is successfully add to this platform</li>
                    </ul>
                </div>
                <?php endif; ?>
                <div class="form-group">
                    <label for="email">Електронска адреса</label>
                    <input type="text" class="form-control" id="email" name="email">
                </div>
                <div class="form-group">
                    <label for="categoryLesson">Категорија</label>
                    <select class="form-control select-color" id="categoryLesson" name="categoryLesson">
                        <option>-- Избери категорија --</option>
                        <?php $__currentLoopData = $categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $category): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <option value="<?php echo e($category->id); ?>"><?php echo e($category->name); ?></option>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                    </select>
                </div>
            </div>
            <div class="modal-footer purple">
                    <button type="submit" class="btn btn-default" id="save">Зачувај</button>
                </form>
                <button type="button" class="btn btn-default" data-dismiss="modal">Затвори</button>
            </div>
        </div>
  
    </div>
</div><?php /**PATH /var/www/html/brainsterprojects/resources/views/layouts/modal.blade.php ENDPATH**/ ?>