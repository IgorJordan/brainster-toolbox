<?php echo $__env->make('layouts.head', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    <body <?php if(Request::is("/")): ?> class="" <?php elseif(Request::is("courses")): ?> class="bkg-color" <?php endif; ?>>
        <nav class="navbar navbar-default nav-custom" role="navigation">
             <!-- Brand and toggle get grouped for better mobile display -->

            
            <?php if(Request::is('brainster/admin/panel')): ?>
            <div class="navbar-header">
                <a class="navbar-brand" href="<?php echo e(route('main')); ?>">
                    <img class="logo" src="<?php echo e(asset('images/brainster_logo.png')); ?>" alt="">
                </a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav navbar-right">
                    <?php if(auth()->guard()->guest()): ?>
                    <li class="last" data-toggle="modal" data-target="#signUpModal"><a href="#">Sign Up / Sign In</a></li>
                    <?php else: ?> 
                    <li class="nav-item dropdown btnLi">
                        <a id="navbarDropdown" class="btn-custom nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                            <?php echo e(Auth::user()->name); ?>

                        </a>

                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="<?php echo e(route('logout')); ?>"
                            onclick="event.preventDefault();
                                            document.getElementById('logout-form').submit();">
                                <?php echo e(__('Logout')); ?>

                            </a>

                            <form id="logout-form" action="<?php echo e(route('logout')); ?>" method="POST" class="d-none">
                                <?php echo csrf_field(); ?>
                            </form>
                        </div>
                    </li>
                    <?php endif; ?>  
                </ul>
            </div><!-- /.navbar-collapse -->
        </nav>
            
            <?php else: ?>
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="<?php echo e(route('main')); ?>">
                    <img class="logo" src="<?php echo e(asset('images/brainster_logo.png')); ?>" alt="">
                </a>
            </div>
        
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav left-nav">
                    <li class="active"><a href="#"><i class="fas fa-code"></i> Programming</a></li>
                    <li><a href="#"><i class="fas fa-database"></i> Data Science</a></li>
                    <li><a href="#"><i class="fas fa-infinity"></i> DevOps</a></li>
                    <li><a href="#"><i class="fas fa-pencil-alt"></i> Design</a></li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <?php if(auth()->guard()->guest()): ?>
                    <li><a href="#" data-toggle="modal" data-target="#signUpModal"><i style="color: blue;" class="fas fa-plus"></i> Submit a tutorial</a></li>
                    <?php else: ?>
                    <li><a href="#" data-toggle="modal" data-target="#submitModal"><i style="color: blue;" class="fas fa-plus"></i> Submit a tutorial</a></li>
                    <?php endif; ?>
                    
                    <?php if(auth()->guard()->guest()): ?>
                    <li class="last" data-toggle="modal" data-target="#signUpModal"><a href="#">Sign Up / Sign In</a></li>
                    <?php else: ?> 
                    <li class="nav-item dropdown btnLi">
                        <a id="navbarDropdown" class="btn-custom nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                            <?php echo e(Auth::user()->name); ?>

                        </a>

                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="<?php echo e(route('logout')); ?>"
                            onclick="event.preventDefault();
                                            document.getElementById('logout-form').submit();">
                                <?php echo e(__('Logout')); ?>

                            </a>

                            <form id="logout-form" action="<?php echo e(route('logout')); ?>" method="POST" class="d-none">
                                <?php echo csrf_field(); ?>
                            </form>
                        </div>
                    </li>
                    <?php endif; ?>  
                </ul>
            </div><!-- /.navbar-collapse -->
        </nav>
       
            <?php if(count($errors) > 0): ?>
                <div class="alert alert-danger text-center" id="errorDiv">
                    <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <p><i class="fas fa-exclamation-circle"></i> <?php echo e($error); ?></p>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </div>
            <?php elseif(session('message')): ?>
                <div class="alert alert-success text-center" id="errorDiv">
                    <p><i class="fas fa-check-circle"></i> <?php echo e(session("message")); ?></p>
                </div>
            <?php endif; ?>

        <?php echo $__env->make('layouts.modal-submit', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
        <?php echo $__env->make('layouts.modal-signup', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
        <?php echo $__env->make('layouts.modal-signin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
        <?php endif; ?><?php /**PATH /var/www/html/brainsterprojects/resources/views/layouts/header.blade.php ENDPATH**/ ?>