<?php $__env->startSection('content'); ?>
    <div class="container content">
        <?php if(auth()->guard()->guest()): ?>
            
        <?php else: ?>
        <div class="row control-buttons">
            <div class="col-md-8 col-sm-8 col-xs-6 btn-font">
                <a class="btn btn-primary" href="<?php echo e(route('addLesson', ['id' => $category->id])); ?>"><b>Додади</b></a><span><b> нова лекција</b></span>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-6 btn-font">
                <a class="btn btn-primary" href="<?php echo e(route('banner')); ?>"><b>Постави</b></a><span><b> нов банер</b></span>
            </div>
        </div>
        <?php endif; ?>
        
        <div class="row test">
            <div class="col-md-8 col-sm-8 col-xs-12">
                <div class="row">
                    
                    <?php $__currentLoopData = $lessons; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $lesson): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <div class="col-md-12 baner">
                        <div class="row"> 
                            <div class="col-md-9 col-sm-9 col-xs-8">
                                <a class="lesson-link" href="#"><p><span>#<?php echo e($counter++); ?> </span><b> <?php echo e($lesson->title); ?> </b></p></a>
                            </div>
                            <div class="col-md-3 col-sm-3 col-xs-4 text-right">
                                <p class="date-size"><i class="<?php echo e($lesson->icon); ?>"></i><span class="date-span"> <?php echo e($lesson->created_at->format('Y-m-d')); ?></span></p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <p class="lesson-text"><?php echo e($lesson->text); ?></p>
                            </div>
                        </div>
                    </div>
                    <div class="btn-group-justified control-buttons">
                        <?php if(auth()->guard()->guest()): ?>
                            
                        <?php else: ?>
                        <a class="btn btn-info" href="<?php echo e(route('editLesson', ['id' => $lesson->id])); ?>"><i class="fas fa-edit"></i><b> Измени</b></a>
                        <a class="btn btn-danger" href="<?php echo e(route('deleteLesson', ['id' => $lesson->id])); ?>"><i class="fas fa-trash-alt"></i><b> Избриши</b></a>
                        <?php endif; ?>
    
                    </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                </div>
            </div>

            <div class="col-md-4 col-sm-4 col-xs-12">
                <div class="baner">

                    <?php $__currentLoopData = $banners; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $banner): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <h3><b><?php echo e($banner->title); ?></b></h3>
                    <p><?php echo e($banner->text); ?></p>
                    
                    <div class="btn-more">
                        <a href="<?php echo e($banner->link); ?>" target="_blank"><p class="more"><b>Повеќе</b><span><i class="fas fa-arrow-right"></i></span></p></a>
                    </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                </div>
            </div>
        </div>
        <?php echo e($lessons->links()); ?>

    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.project', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/html/brainsterprojects/resources/views/lessons.blade.php ENDPATH**/ ?>