<?php $__env->startSection('content'); ?>
<div class="container">
    <div class="row justify-content-center" style="margin-top:50px">
        <div class="col-md-12 ">
            <table class="table table-bordered">
                <thead>
                <tr class="table-header">
                    <th>Course</th>
                    <th>Status</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                <?php $__currentLoopData = $courses; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $course): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <tr class="<?php echo e($course->status); ?>">
                        <td>
                            <div class="course-card course-side">
                                <div class="col-md-1">
                
                                    <div class="btn-group square" data-toggle="buttons">
                                        <label class="btn btn-primary active">
                                            <input type="checkbox" name="" id="" autocomplete="off">
                                            <i class="fas fa-caret-up"></i>
                                            <h4><?php echo e($course->like); ?></h4>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-11 course-pad">
                                    <a class="course-title" href="<?php echo e($course->url); ?>" target="_blank">
                                        <h4><?php echo e($course->title); ?> 
                                            <span class="grey">(<?php echo e($course->platform); ?>)</span>
                                        </h4>
                                    </a>
                                    <div class="info-part">
                                        <span><a href=""><i class="fas fa-bookmark"></i><b> Bookmark</b></a></span>
                                        <span>Submited by <a href=""><b><?php echo e($course->user->name); ?></b></a></span>
                                        <span><i class="fas fa-genderless"></i></span>
                                        <span>Views</span>
                                        <span><i class="fas fa-genderless"></i></span>
                                        <span>Discuss</span>
                                    </div>
                                    <div class="tags">
                                        <a href=""><b><?php echo e($course->type->name); ?></b></a>
                                        <a href=""><b><?php echo e($course->level->name ?? ''); ?></b></a>
                                        <a href=""><b><?php echo e($course->medium->name ?? ''); ?></b></a>
                                        <a href=""><b><?php echo e($course->version->name ?? ''); ?></b></a>
                                    </div>
                                </div>
                            </div>
                        </td>
                        <td><b><?php echo e($course->status); ?></b></td>
                        <td class="buttons">
                            <?php if($course->status == 'unresponded'): ?>
                                <a href="<?php echo e(route('reject', ['id' => $course->id])); ?>" class="btn btn-md btn-danger btn-block">Reject</a>
                                <a href="<?php echo e(route('accept', ['id' => $course->id])); ?>" class="btn btn-md btn-success btn-block">Accept</a>
                            <?php elseif($course->status == 'accepted'): ?>
                                <a href="<?php echo e(route('reject', ['id' => $course->id])); ?>" class="btn btn-md btn-danger btn-block">Reject</a>
                                <a href="<?php echo e(route('delete', ['id' => $course->id])); ?>" class="btn btn-md btn-danger btn-block">Delete</a>
                            <?php elseif($course->status == 'rejected'): ?>
                                <a href="<?php echo e(route('accept', ['id' => $course->id])); ?>" class="btn btn-md btn-success btn-block">Accept</a>
                                <a href="<?php echo e(route('delete', ['id' => $course->id])); ?>" class="btn btn-md btn-danger btn-block">Delete</a>
                            <?php endif; ?>
                        </td>
                    </tr>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </tbody>
            </table>
        </div>
        <?php echo e($courses->links()); ?>

    </div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.project', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/html/brainsterprojects/resources/views/admin-panel.blade.php ENDPATH**/ ?>