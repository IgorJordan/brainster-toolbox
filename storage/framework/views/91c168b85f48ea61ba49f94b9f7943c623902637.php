<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        
        <?php if(Request::is('/')): ?>
        <title>Brainster | Technologies</title>
        <?php elseif(Request::is('courses/*')): ?>
        <title>Brainster | <?php echo e($tech->name); ?> courses and tutorials</title>
        <?php elseif('brainster/admin/panel'): ?>
        <title>Brainster | Admin's panel</title>
        <?php endif; ?>
        
        
        <link rel="stylesheet" href="<?php echo e(asset('css/style.css')); ?>">
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
        <script src="https://kit.fontawesome.com/a076d05399.js"></script>
    </head><?php /**PATH /var/www/html/brainsterprojects/resources/views/layouts/head.blade.php ENDPATH**/ ?>