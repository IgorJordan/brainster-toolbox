<div id="signInModal" class="modal fade new-modal" role="dialog">
    <div class="modal-dialog">
  
      <!-- Modal content-->

        <div class="modal-content">

            <div class="modal-body text-center">
                <button type="button" class="close modal-close m-signin" data-dismiss="modal"><i class="fas fa-times"></i></button>
                <h3 class="modal-title"><b>Welcome back</b></h3>
                <p class="sub grey">Continue with:</p>
                <div class="line"></div>

                <div class="social-btn sub">
                    <a href="" class="btn btn-google" role="button"><i class="fab fa-google"></i> Sign In with Google</a>
                </div>
                <div class="second-row sub">
                    <div class="social"><a href="" class="btn btn-facebook" role="button"><i class="fab fa-facebook-square"></i> Facebook</a></div>
                    <div class="social"><a href="" class="btn btn-github" role="button"><i class="fab fa-github"></i> Github</a></div>
                </div>

                <div class="second-row sub">
                    <div class="span-line"></div>
                    <div class="or"><p class="grey">OR</p></div>
                    <div class="span-line"></div>
                </div>

                <form action="<?php echo e(route('login')); ?>" method="POST" class="sub">
                <?php echo csrf_field(); ?>

                <div class="form-group">
                    <input type="email" class="form-control" id="email" name="email" placeholder="E-mail address">
                </div>

                <div class="form-group">
                    <input type="password" class="form-control" id="password" name="password" placeholder="Password">
                </div>

                    <button onClick="myFunction()" type="submit" class="form-control btn btn-submit sub" id="signIn"><b>Login</b></button>
                </form>

                <p class="sub">Don't have an account?<a href="#signUpModal" data-toggle="modal" data-dismiss="modal"> Sign Up</a></p>
            </div>
        </div>
  
    </div>
</div>
<?php /**PATH /var/www/html/brainsterprojects/resources/views/layouts/modal-signin.blade.php ENDPATH**/ ?>