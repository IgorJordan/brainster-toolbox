<p>Dear <em><?php echo e($username); ?></em>,</p>
<br>
<p>please be inform that your request for adding tutorial/course to the platform was <span style="color: <?php echo e($style); ?>;"><?php echo e($status); ?></span>.</p>
<br>
<p>Thank you for using our Brainster Tools program.</p>
<br>
<p>Best regards,</p>
<br>
<p><em><b>Brainster Team</b></em></p>

<?php /**PATH /var/www/html/brainsterprojects/resources/views/email/confirmation.blade.php ENDPATH**/ ?>