<?php $__env->startSection('content'); ?>
<div class="container add-margin">
    <div class="row">
        <div class="col-md-12">
            <ul class="nav nav-tabs">
                <li role="presentation"><a href="<?php echo e(URL::previous()); ?>">Назад</a></li>
                <li role="presentation" class="active"><a class="active" href="#">Додади лекција</a></li>
            </ul>
        </div>
    </div>
    <section class="bg-white">
    <div class="row">
        <div class="col-md-12">
            <h2 class="text-center">Додади нова лекција:</h2>
        </div>
    </div>
    <div class="row">
        <div class="col-md-8 col-md-offset-2 project-form">
            <form action="<?php echo e(route('storeLesson', ['id' => $category->id])); ?>" method="POST">
                <?php echo csrf_field(); ?>
                <?php if(count($errors) > 0): ?>
                <div class="alert alert-danger">
                    <ul>
                    <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <li><?php echo e($error); ?></li>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </ul>
                </div>
                <?php elseif(Request::is()): ?>
                <div class="alert alert-success">
                    <ul>
                        <li>The lesson is successfully add to this platform</li>
                    </ul>
                </div>
                <?php endif; ?> 
                <div class="form-group">
                    <label for="title">Наслов</label>
                    <input type="text" name="title" class="form-control" id="title">
                </div>
                <div class="form-group">
                    <label for="icon">Икона</label>
                    <input type="text" name="icon" class="form-control" id="icon" placeholder="">
                </div>
                <div class="form-group">
                    <label for="text">Текст</label>
                    <textarea name="text" class="form-control" id="text" rows="2"></textarea>
                </div>

                <button type="submit" class="btn btn-block btn-default btn-color"><b>Додај</b></button>
              </form>
        </div>
    </div>
    </section>
</div>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.project', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/html/brainsterprojects/resources/views/add-lesson.blade.php ENDPATH**/ ?>