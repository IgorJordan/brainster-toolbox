<?php $__env->startSection('content'); ?>
<div class="container course-page">
    <div class="row">
        <div class="col-md-3 side-bar">
            <p class="side-h"><b>Filter Courses</b></p>

            <p><b>Type of course</b></p>
            <?php $__currentLoopData = $types; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $type): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <div class="checkbox filterCourse">
                <label for="" id="<?php echo e($type->attribute); ?>">
                    <input type="checkbox" name="" > <?php echo e($type->name); ?> 
                    (<span></span>)
                </label>
            </div>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

            <p><b>Medium</b></p>
            <?php $__currentLoopData = $media; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $medium): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <div class="checkbox filterCourse">
                <label for="" id="<?php echo e($medium->attribute); ?>">
                    <input type="checkbox" name=""> <?php echo e($medium->name); ?> 
                    (<span></span>)
                </label>
            </div>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

            <p><b>Level</b></p>
            <?php $__currentLoopData = $levels; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $level): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <div class="checkbox filterCourse">
                <label for="" id="<?php echo e($level->attribute); ?>">
                    <input type="checkbox" name=""> <?php echo e($level->name); ?> 
                    (<span></span>)
                </label>
            </div>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            
            <p><b>Version</b></p>
            <?php $__currentLoopData = $versions; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $version): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <div class="checkbox filterCourse" id="versionId">
                <label for="" id="<?php echo e($version->attribute); ?>">
                    <input type="checkbox" name=""> <?php echo e($version->name); ?> 
                    (<span></span>)
                </label>
            </div>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>    
        </div>

        <div id="courseList" class="col-md-9">
            <div class="title course-side">
                <p><b>Top <?php echo e($tech->name); ?> tutorials</b></p>
                <div class="filter-butons">
                    <a href="">Upvotes</a>
                    <a class="btn-2" href="">Recent</a>
                </div>
            </div>
            <?php $__currentLoopData = $courses; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $course): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <div class="course-card course-side <?php echo e($course->type->attribute); ?> <?php echo e($course->level->attribute ?? ''); ?> <?php echo e($course->medium->attribute ?? ''); ?> <?php echo e($course->version->attribute ?? ''); ?>">
                <div class="col-md-1">

                    <div class="btn-group square" data-toggle="buttons">
                        <label class="btn btn-primary active">
                            <input type="checkbox" name="" id="" autocomplete="off">
                            <i class="fas fa-caret-up"></i>
                            <h4><?php echo e($course->likes->count()); ?></h4>
                        </label>
                    </div>
                </div>
                <div class="col-md-11 course-pad">
                    <a class="course-title" href="<?php echo e($course->url); ?>" target="_blank">
                        <h4><?php echo e($course->title); ?> 
                            <span class="grey">(<?php echo e($course->platform); ?>)</span>
                        </h4>
                    </a>
                    <div class="info-part">
                        <span><a href=""><i class="fas fa-bookmark"></i><b> Bookmark</b></a></span>
                        <span>Submited by <a href=""><b><?php echo e($course->user->name); ?></b></a></span>
                        <span><i class="fas fa-genderless"></i></span>
                        <span>Views</span>
                        <span><i class="fas fa-genderless"></i></span>
                        <span>Discuss</span>
                    </div>
                    <div class="tags">
                        <a href=""><b><?php echo e($course->type->name); ?></b></a>
                        <a href=""><b><?php echo e($course->level->name ?? ''); ?></b></a>
                        <a href=""><b><?php echo e($course->medium->name ?? ''); ?></b></a>
                        <a href=""><b><?php echo e($course->version->name ?? ''); ?></b></a>
                    </div>
                </div>
            </div>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            <div class="padding text-center" id="padBtn">
                <a href="#" class="prev btn btn-pad">Previous page</a>
                <a href="#" class="next btn btn-pad">Next page</a>
            </div>
        </div> 
    </div>
    <div class="row opt">
        <div class="col-md-9 col-md-offset-3">
            <div class="row">
                <h4 style="margin-bottom: 4vh;">You might also be interested in:</h4>
            </div>
            <div class="row">
                <?php $__currentLoopData = $techesLimit; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $tech): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <a href="<?php echo e(route('courses', ['id' => $tech->id])); ?>">
                <div class="col-md-4">
                    <div class="card">
                        <img class="card-logo" src="<?php echo e(asset("images/logos/$tech->logo")); ?>" alt="program_image">
                        <span> <?php echo e($tech->name); ?></span>
                    </div>
                </div>
                </a>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.project', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/html/brainsterprojects/resources/views/courses.blade.php ENDPATH**/ ?>