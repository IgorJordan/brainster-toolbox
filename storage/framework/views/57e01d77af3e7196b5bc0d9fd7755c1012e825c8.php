<?php $__env->startSection('content'); ?>
<div class="container">
    <div class="row">
        <div class="col-md-12 text-center">
            <h1>Find the Best Programming Courses & Tutorials</h1>
        </div>
    </div>
    <div class="row search-section">
        <div class="col-md-12 form-group">
            <i class="fas fa-search icon"></i>
            <input type="text" class="form-control search-input" name="search" id="search" placeholder="Search for the language you want to learn: Python, JavaScript...">
        </div>
    </div>
    <div class="row card-section" id="searchList">
        <?php $__currentLoopData = $teches; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $tech): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <a href="<?php echo e(route('courses', ['id' => $tech->id])); ?>">
        <div class="col-md-4">
            <div class="card">
                <img class="card-logo" src="<?php echo e(asset("images/logos/$tech->logo")); ?>" alt="program_image">
                <span> <?php echo e($tech->name); ?></span>
            </div>
        </div>
        </a>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </div>
</div>
<?php $__env->stopSection(); ?>
        
        
<?php echo $__env->make('layouts.project', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/html/brainsterprojects/resources/views/index.blade.php ENDPATH**/ ?>