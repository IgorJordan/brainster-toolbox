CREATE TABLE cards (
    id INT UNSIGNED AUTO_INCREMENT,
    id_name VARCHAR(16),
    category_class VARCHAR(16),
    category_name VARCHAR(32),
    time_frame_class VARCHAR(16),
    time_frame_name VARCHAR(32),
    group_size_class VARCHAR(32),
    group_size_name VARCHAR(32),
    title VARCHAR(64),
    img_path VARCHAR(32),
    logo_path VARCHAR(32),
    facilate_level VARCHAR(32),
    materials VARCHAR(64),
    description VARCHAR(8000),

    PRIMARY KEY (id)
)

CREATE TABLE visitor (
    id INT UNSIGNED AUTO_INCREMENT,
    visitor_email VARCHAR(64),

    PRIMARY KEY (id)
)

CREATE TABLE employee_option (
    id INT UNSIGNED AUTO_INCREMENT,
    employee_num VARCHAR(32),

    PRIMARY KEY (id)
)

CREATE TABLE sector_option (
    id INT UNSIGNED AUTO_INCREMENT,
    sector VARCHAR(32),

    PRIMARY KEY (id)
)

CREATE TABLE register_table (
    id INT UNSIGNED AUTO_INCREMENT,
    name VARCHAR(16),
    surname VARCHAR(32),
    company VARCHAR(32),
    email VARCHAR(64),
    phone VARCHAR(32),
    employee_num VARCHAR(32),
    sector_id VARCHAR(32),
    message VARCHAR(2000),

    PRIMARY KEY (id)
)