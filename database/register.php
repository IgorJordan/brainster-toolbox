<?php

require_once 'MysqlConnection.php';

$db = new MySql();

$sql = "INSERT INTO register_table (name, surname, company, email, phone, employee_num, sector_id, message)
        VALUES (:name, :surname, :company, :email, :phone, :employee_num, :sector_id, :message)";

$params = [
    "name" => $_POST["name"],
    "surname" => $_POST["surname"],
    "company" => $_POST["company"],
    "email" => $_POST["email"],
    "phone" => $_POST["phone"],
    "employee_num" => $_POST["employeeNum"],
    "sector_id" => $_POST["sector"],
    "message" => $_POST["message"]
];

$db->db_write($sql, $params);
header("Location:http://localhost/brainsterprojects/index.php?success");
die();
