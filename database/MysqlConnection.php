<?php

class MySql
{
    private $connection;


    public function __construct(){
        try{
            $host ="localhost";
            $username = "root";
            $password = "root";
            $dbname = "tool_box";
            $options = [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION];
            $this->connection = new PDO("mysql:host=$host;dbname=$dbname;charset=utf8", $username, $password, $options);

        }catch(PDOException $e){
            echo $e->getMessage();
            die();
        }
    }

    public function connection(){
        return $this->connection;
    }

    public function db_read($sql, $params = null){
        $stmt = $this->connection->prepare($sql);
        if($params){
            $stmt->execute($params);
        }else{
            $stmt->execute();
        }
        
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

        return $result;
    }


    public function db_write($sql, $params){
        $stmt = $this->connection->prepare($sql);
        $stmt->execute($params);

        $result = $this->connection->lastInsertId();

        return $result;
    }
}