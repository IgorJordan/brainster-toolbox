<?php

require_once 'MysqlConnection.php';


$visitor = $_POST["emailVisitor"];

if(empty($visitor))
{
    header("Location:http://localhost/brainsterprojects/index.php?empty-input");
    die();
} elseif(!filter_var($visitor, FILTER_VALIDATE_EMAIL))
{
    header("Location:http://localhost/brainsterprojects/index.php?invalid-email");
    die();
}

$db = new MySql();

$sql = "INSERT INTO visitor (visitor_email) VALUE (:visitor_email)";

$params = [
    "visitor_email" => $_POST["emailVisitor"],
];

$db->db_write($sql, $params);

