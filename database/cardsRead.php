<?php

require_once 'MysqlConnection.php';

$db = new MySql();
$sql = "SELECT * FROM cards";
$cards = $db->db_read($sql);


foreach($cards as $card) {
?>
<div class="col-md-4 all <?= $card['category_class'] ?> <?= $card['group_size_class'] ?> <?= $card['time_frame_class'] ?>">
    <div id="<?= $card['id_name'] ?>" class="thumbnail card-border" data-toggle="modal" data-target="#visitorModal">
        <img class="card-image" src="<?= $card['img_path']?>" alt="card image">
        <h4><strong><?= $card['title'] ?></strong>
            <span>
                <img src="<?= $card['logo_path'] ?>" width="40px" style="background-color: black" class="img-circle icon" alt="logo">
            <span>
        </h4>
        <p>Категорија: <span class="blue"><?= $card['category_name']?></span></p>

        <p class="card-img">
            <span class="glyphicon glyphicon-time"></span>
            <div class="card-text">    
                <h4><strong> Времетраење</strong></h4>
                <p><?= $card['time_frame_name'] ?></p>
            </div>   
        </p>
    </div>
</div>
<?php
}


