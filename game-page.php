<?php
require_once 'html/head.php';
?>

    <header class="bg-image" style="background-image: url(images/01.jpg);">
        <!-- navigation section -->
        <?php
        require_once 'html/navigation.php';
        ?>


    <div class="diagonal"></div>
    </header>

    <main class="main-color-gp">
        <div class="container">
            <div class="row">
                <h1>Naslov</h1>
            </div>
            <div class="row table-title">
                <div class="col-md-3 column-text">
                    <span class="far fa-clock" aria-hidden="true"></span>
                    <div class="div-icon">
                        <h4><strong>Time Frame</strong></h4>
                        <p>3h 20m</p>
                    </div>
                </div>
                <div class="col-md-3 column-text">
                    <span class="fas fa-user-friends" aria-hidden="true"></span>
                    <div class="div-icon">
                        <h4><strong>Group Size</strong></h4>
                        <p>2-30</p>
                    </div>
                </div>
                <div class="col-md-3 column-text">
                    <span class="fas fa-university" aria-hidden="true"></span>
                    <div class="div-icon">
                        <h4><strong>Facilitation Level</strong></h4>
                        <p>2-30</p>
                    </div>
                </div>
                <div class="col-md-3 column-text">
                    <span class="fa fa-clone" aria-hidden="true"></span>
                    <div class="div-icon">
                        <h4><strong>Materials</strong></h4>
                        <p>2-30</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <p></p>
            </div>
        </div>


    </main>
<!-- footer section -->
<?php
require_once 'html/footer.php';
?>