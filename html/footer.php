        <footer class="purple">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12 text-center text2">
                        <h1>FUTURE-PROOF YOUR ORGANIZATION</h1>
                        <p>Find out how to unlock progress in your business. Understand your current state, identify<br>
                            opportunity areas and prepare to take charge of your organization's future.</p>
                        <p><button class="btn btn-colorY assessment" onClick="window.location.href = '​https://brainsterquiz.typeform.com/to/FfnWVC'">Take the assessment</button></p>
                    </div>
                </div>
            </div>


            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-4 text-center href-span">
                        <span><a href="">About us</a></span>
                        <span><a href="">Contact</a></span>
                        <span><a href="">Gallery</a></span>
                    </div>
                    <div class="col-md-4">
                        <p class="text-center">
                            <a href="" class="sellectA"><img src="images/logo.png" alt="" class="img-responsive logo" width="150px"></a>
                        </p>
                    </div>
                    <div class="col-md-4 text-center href-icon">
                        <span><a href="https://www.linkedin.com/school/brainster-co/" target="_blank"><i class="fa fa-linkedin" aria-hidden="true"></i></a></span>
                        <span><a href="https://twitter.com/BrainsterCo" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a></span>
                        <span><a href="https://www.facebook.com/brainster.co" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a></span>
                    </div>
                </div>
            </div>
        </footer>


        <script src="js/card.js"></script>
        <script
			  src="https://code.jquery.com/jquery-3.4.1.min.js"
			  integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
			  crossorigin="anonymous"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <!-- Bootstrap JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    </body>
</html>