<?php
require_once 'html/head.php';
?>

    <header class="purple" id="up">
        <!-- navigation section -->
        <?php
        require_once 'html/navigation.php';
        ?>

            <div class="col-md-12 text-center">
                <p class="text1">Изработено од студенти на академијата за програмирање на <span><a style="color: #ffde33" href="">Brainster</a></span></p>
            </div>
            <!-- tittle section -->
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12 text-center text2">
                        <h1>FUTURE-PROOF YOUR ORGANIZATION</h1>
                        <p>Find out how to unlock progress in your business. Understand your current state, identify<br>
                            opportunity areas and prepare to take charge of your organization's future.</p>
                        <p><button class="btn btn-colorY assessment" onClick="window.location = '​https://brainsterquiz.typeform.com/to/kC2I9E​'">Take the assessment</button></p>
                    </div>
                </div>
            </div>
            <!-- buttons section -->
            <div class="container-fluid">
                <div class="row" style="padding: 20px">
                    <div class="col-md-6 btnCtg filterBtn">
                        <div class="row">
                            <p>Browse by Category</p>
                        </div>
                        <div class="row">
                            <button  onclick="setActive('energizers', this)" type="button" id="energizers" class="btn btn-default three-btns">ENERGIZERS</button>
                            <button onclick="setActive('innovation', this)" type="button" id="innovation" class="btn btn-default three-btns">INNOVATION</button>
                            <button onclick="setActive('leadership', this)" type="button" id="leadership" class="btn btn-default three-btns">SELF-LEADERSHIP</button>
                        </div>
                        <div class="row">
                            <button onclick="setActive('action', this)" type="button" id="action" class="btn btn-default three-btns">ACTION</button>
                            <button onclick="setActive('team', this)" type="button" id="team" class="btn btn-default three-btns">TEAM</button>
                            <button onclick="setActive('all', this)" type="button" id="all" class="btn btn-default three-btns">ALL</button>
                        </div>
                    </div>

                    <div class="col-md-2 filterBtn">
                        <div class="row">
                            <p>Time frame (minutes)</p>
                        </div>
                        <div class="row">
                            <button onclick="setActive('min30', this)" type="button" id="min30" class="btn btn-default middle-btns">5-30</button>
                            <button onclick="setActive('min60', this)" type="button" id="min60" class="btn btn-default middle-btns">30-60</button>
                        </div>
                        <div class="row">
                            <button onclick="setActive('min120', this)" type="button" id="min120" class="btn btn-default middle-btns">60-120</button>
                            <button onclick="setActive('min240', this)" type="button" id="min240" class="btn btn-default middle-btns">120-240</button>
                        </div>
                    </div>

                    <div class="col-md-2 btnCtg filterBtn">
                        <div class="row">
                            <p>Group size</p>
                        </div>
                        <div class="row">
                            <button onclick="setActive('group10', this)" type="button" id="group10" class="btn btn-default three-btns">2-10</button>
                            <button onclick="setActive('group40', this)" type="button" id="group40" class="btn btn-default three-btns">10-40</button>
                            <button onclick="setActive('group40up', this)" type="button" id="group40up" class="btn btn-default three-btns">40+</button>
                        </div>
                    </div>

                    <div class="col-md-2 search-btn-pos filterBtn">
                        <div class="row">
                            <button type="submit" id="search" class="btn btn-default search-btn">SEARCH</button>
                        </div>

                    </div>
                </div>
            </div>

        </header>

        <main class="main-color">
            <!-- cards section -->
            <div class="container-fluid" style="padding: 100px 0;">
                <div class="row" style="padding: 0 30px">
                    <?php
                    require_once 'database/cardsRead.php';
                    ?>
                </div>
                <div class="modal fade" id="visitorModal" role="dialog">
                    <div class="modal-dialog modal-md">
                        <div class="modal-content">
                            <div class="modal-header modal-color">
                                <button type="button" class="close" data-dismiss="modal" style="color: white">&times;</button>
                                <h4 class="modal-title text-center">Внесете ја вашата електронска адреса за да ја видите содржината на играта:</h4>
                            </div>
                            <div class="modal-body">
                                <form action="database/modal.php" method="POST">
                                    <div class="form-group">
                                        <label for="emailVisitor">Твојата електронска адреса:</label>
                                        <span id="alertFrom" style="color: red;"></span>
                                        <input class="form-control" id="visitorEmail" name="emailVisitor" value="">
                                    </div>

                                    <p class="text-center"><button name="submit" type="submit" class="btn btn-colorY">Потврди</button></p>
                                    <span id="alertForm" style="color: red;"></span>
                                </form>
                            </div>
                            <div class="modal-footer modal-color"></div>
                        </div>
                    </div>
                </div>
            </div>

        </main>
    <!-- footer section -->
    <?php
    require_once 'html/footer.php';
    ?>
