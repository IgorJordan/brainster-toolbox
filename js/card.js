function openNav() {
    let sidebar = document.getElementById("mySidenav");
    sidebar.style.width = "30%";
    sidebar.style.outline = "0";
}

function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
    document.getElementById("bodyId").style.opacity = "unset";
}


let energizers = document.getElementById('energizers').addEventListener('change', handle1);
let innovation = document.getElementById('innovation');
let leadership = document.getElementById('leadership');
let action = document.getElementById('action');
let team = document.getElementById('team');
let all = document.getElementById('all');
let min30 = document.getElementById('min30');
let min60 = document.getElementById('min60');
let min120 = document.getElementById('min120');
let min240 = document.getElementById('min240');
let group10 = document.getElementById('group10');
let group40 = document.getElementById('group40');
let group40up = document.getElementById('group40up');

let energizeCards = document.getElementsByClassName('energizer')

function handle1() {
    hideAll();

    if(energizers.click == false 
        && innovation.click == false
        && leadership.click == false
        && action.click == false
        && team.click == false
        && all.click == false
        && min30.click == false
        && min60.click == false
        && min120.click == false
        && min240.click == false
        && group10.click == false
        && group40.click == false
        && group40up.click == false)
        {
            showSome('.all');

        }

    if(energizers.click == true) {
        innovation.click == false
        leadership.click == false
        action.click == false
        team.click == false
        all.click == false
        min30.click == false
        min60.click == false
        min120.click == false
        min240.click == false
        group10.click == false
        group40.click == false
        group40up.click == false
        showSome('.energizer')
    }
}
