<?php
require_once 'database/option.php';
require_once 'html/head.php';
?>
<header class="purple">
    <nav class="navbar navbar-default" role="navigation">
        <div class="container-fluid">
            <div class="col-md-4 col-md-offset-4">
                <div class="center-logo">
                    <a href="" class="sellectA"><img src="images/logo.png" alt="" class="img-responsive logo" width="150px"></a>
                </div>
            </div>
        </div>
    </nav>
</header>
<main>
    <div class="container">
        <div class="row">
            <h2 class="text-center">Регистрирај се на нашата страна</h2>
            <div class="col-md-8 col-md-offset-2 register-page">
            <form action="database/register.php" method="POST">
                    <div class="form-group">
                        <label for="name">Име:</label>

                        <input class="form-control" id="name" name="name" value="">
                    </div>
                    <div class="form-group">
                        <label for="surname">Презиме:</label>

                        <input class="form-control" id="surname" name="surname" value="">
                    </div>
                    <div class="form-group">
                        <label for="company">Компанија:</label>

                        <input class="form-control" id="company" name="company" value="">
                    </div>
                    <div class="form-group">
                        <label for="email">Електронска адреса:</label>

                        <input class="form-control" id="email" name="email" value="">
                    </div>
                    <div class="form-group">
                        <label for="phone">Телефонски број:</label>

                        <input class="form-control" id="phone" name="phone" value="">
                    </div>
                    <div class="form-group">
                        <label for="employeeNum">Број на вработени</label>
                        <select class="form-control" name="employeeNum" id="employeeNum">
                            <option value=""></option>
                            <?php
                            foreach($employee as $num){
                            ?>
                            <option value="<?= $num['id']?>"><?= $num['employee_num']?></option>

                            <?php
                            }
                            ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="sector">Сектор</label>
                        <select class="form-control" name="sector" id="sector">
                            <option value=""></option>
                            <?php
                            foreach($sector as $job){
                            ?>
                            <option value="<?= $job['id']?>"><?= $job['sector']?></option>

                            <?php
                            }
                            ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="message">Порака</label>
                        <textarea class="form-control" name="message" id="" rows="5">
                        </textarea>
                    </div>

                    <p class="text-center"><button name="submit" type="submit" class="btn btn-colorY">Потврди</button></p>
                    <span id="alertForm" style="color: red;"></span>
                </form>
            </div>                
        </div>
    </div>
</main>

        <script src="js/card.js"></script>
        <script
			  src="https://code.jquery.com/jquery-3.4.1.min.js"
			  integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
			  crossorigin="anonymous"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <!-- Bootstrap JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    </body>
</html>